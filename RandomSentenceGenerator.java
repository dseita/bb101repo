/*
 * Daniel Seita's code to generate random sentences (with fancy stuff like prepositional phrases!)
 * (c) 2012 Daniel Seita
 * Status: Incomplete
 * I know the code is messy, I'm just trying to get something done in the shortest time
 */

import java.io.*;
import java.util.*;

public class RandomSentenceGenerator {
    public static void main(String[] args) throws IOException {

        // The data sets I'm using ... these may be easily modified
        // nounPhrase, adjectivePhrase, and verbPhrase are composed of nonterminals
	ArrayList<String> nounPhrase = new ArrayList<String>(Arrays.asList("DP-ADJP-N","PN"));
	ArrayList<String> definitePhrase = new ArrayList<String>(Arrays.asList("a","the"));
	ArrayList<String> adjectivePhrase = new ArrayList<String>(Arrays.asList("ADJ","ADJ-ADJP"));
	ArrayList<String> verbPhrase = new ArrayList<String>(Arrays.asList("TV-NP","IV"));
	ArrayList<String> adjective = new ArrayList<String>(Arrays.asList("big","fat","quiet","wonderful","faulty","mysterious","dark","bright","dazzling"));
	ArrayList<String> noun = new ArrayList<String>(Arrays.asList("dog","cat","man","mother","child","girl","machine","robot","device","music box"));
	ArrayList<String> properNoun = new ArrayList<String>(Arrays.asList("John","Jane","Sally","Daniel","Elliot","Thomas","Rebecca","Will","Chris","Lily","Jacob","Dylan","Caitlin","Ben"));
	ArrayList<String> transitiveVerb = new ArrayList<String>(Arrays.asList("hit","honored","kissed","helped","created","destroyed","handled","killed"));	
	ArrayList<String> intransitiveVerb = new ArrayList<String>(Arrays.asList("died","collapsed","laughed","wept","arrived","ate","sneezed"));

	// Use the two below for unique words only
	//ArrayList<String> prepPhraseStart = new ArrayList<String>(Arrays.asList("before midnight ,","in that large building ,", "after several hours ,", "according to witnesses ,", "on planet Earth ,"));
	//ArrayList<String> prepPhraseEnd = new ArrayList<String>(Arrays.asList("on that hill", "by that old house", "about our garden", "behind Darius", "until dawn"));

	ArrayList<String> prepPhraseStart = new ArrayList<String>(Arrays.asList("before midnight ,", "in the dark building ,", "after a few hours ,", "according to witnesses ,", "on the planet Earth ,"));
	ArrayList<String> prepPhraseEnd = new ArrayList<String>(Arrays.asList("on the hill", "by the dazzling house", "about the garden", "behind Darius", "until dawn"));
	ArrayList<String> prepPhraseMiddle =  new ArrayList<String>(Arrays.asList("with my umbrella ", "in the car ", "through the woods ", "across the river ", "on a helicopter "));

	// Generates 500 (or 200 for tuning purposes) normal English sentences
	for (int i = 1; i <= 200; i++) {

	    boolean repeatADJP = true;               // Prevents excessive repetition of adjectives
	    boolean repeatBecause = true;            // Prevents excessive repetition of "because"

	    // Every 4th sentence will get a starting prepositional phrase
	    if (i % 4 == 0) {
		Random startPrep = new Random();
		System.out.print(prepPhraseStart.get(startPrep.nextInt(prepPhraseStart.size())) + " ");
	    }
	    generateNounPhrase(nounPhrase, definitePhrase, adjectivePhrase, noun, properNoun, adjective, repeatADJP);

	    if ((i + 2) % 4 == 0) {
		Random middlePrep =  new Random();
		System.out.print(", " + prepPhraseMiddle.get(middlePrep.nextInt(prepPhraseMiddle.size())) + ", ");
	    }

	    generateVerbPhrase(verbPhrase, transitiveVerb, intransitiveVerb, nounPhrase, definitePhrase, adjectivePhrase, noun, properNoun, adjective, repeatADJP, repeatBecause);
	    if ((i + 1) % 4 == 0) {
		Random endPrep = new Random();
		System.out.print(prepPhraseEnd.get(endPrep.nextInt(prepPhraseEnd.size())) + " ");
	    }
	    System.out.print(".\n");
	}
    }

    // Methods ...

    public static void generateNounPhrase(ArrayList<String> np, ArrayList<String> dp, ArrayList<String> ap, ArrayList<String> n, ArrayList<String> pn, ArrayList<String> adj, boolean repeatADJP) {
	Random npRand = new Random();
	if (np.get(npRand.nextInt(np.size())).equals("DP-ADJP-N")) {
	    generateDefinitePhrase(dp);
	    generateAdjectivePhrase(ap, adj, repeatADJP);
	    generateNoun(n);
	} else {
	    generateProperNoun(pn);
	}
    }

    public static void generateDefinitePhrase(ArrayList<String> dp) {
	Random dpRand = new Random();
	System.out.print(dp.get(dpRand.nextInt(dp.size())) + " ");
    }

    public static void generateAdjectivePhrase(ArrayList<String> ap, ArrayList<String> adj, boolean repeatADJP) {
	// Note ... I only want one level of recursion, i.e. no adj after adj, etc.
	Random adjRand = new Random();
	if (repeatADJP) {
	    if (ap.get(adjRand.nextInt(ap.size())).equals("ADJ")) {
		generateAdjective(adj);
	    } else {
		repeatADJP = false;
		generateAdjective(adj);
		generateAdjectivePhrase(ap, adj, repeatADJP);
	    }
	} else {
	    generateAdjective(adj);
	}
    }
    
    public static void generateNoun(ArrayList<String> n) {
	Random nounR = new Random();
	System.out.print(n.get(nounR.nextInt(n.size())) + " ");
    }
    
    public static void generateProperNoun(ArrayList<String> pn) {
	Random propR = new Random();
	System.out.print(pn.get(propR.nextInt(pn.size())) + " ");
    }

    public static void generateAdjective(ArrayList<String> adj) {
	Random adjectiveR = new Random();
	System.out.print(adj.get(adjectiveR.nextInt(adj.size())) + " ");
    }

    public static void generateVerbPhrase(ArrayList<String> vp, ArrayList<String> tv, ArrayList<String> iv, ArrayList<String> np, ArrayList<String> dp, ArrayList<String> adjp, ArrayList<String> n, ArrayList<String> pn, ArrayList<String> adj, boolean repeatADJP, boolean repeatBecause) {
	Random vpRand = new Random();
	if (vp.get(vpRand.nextInt(vp.size())).equals("TV-NP")) {
	    generateTransitiveVerb(tv);
	    generateNounPhrase(np, dp, adjp, n, pn, adj, repeatADJP);
	} else {
	    generateIntransitiveVerb(vp, tv, iv, np, dp, adjp, n, pn, adj, repeatADJP, repeatBecause);
	}
    }

    public static void generateTransitiveVerb(ArrayList<String> tv) {
	Random tverbR = new Random();
	System.out.print(tv.get(tverbR.nextInt(tv.size())) + " ");
    }

    public static void generateIntransitiveVerb(ArrayList<String> vp, ArrayList<String> tv, ArrayList<String> iv, ArrayList<String> np, ArrayList<String> dp, ArrayList<String> adjp, ArrayList<String> n, ArrayList<String> pn, ArrayList<String> adj, boolean repeatADJP, boolean repeatBecause) {
	Random iverbR = new Random();
	if (repeatBecause) {
	    Random again = new Random();
	    if (again.nextInt(2) == 1) {
		System.out.print(iv.get(iverbR.nextInt(iv.size())) + " because " );
		generateProperNoun(pn);
		repeatBecause = false;
		generateVerbPhrase(vp, tv, iv, np, dp, adjp, n, pn, adj, repeatADJP, repeatBecause);
	    } else {
		System.out.print(iv.get(iverbR.nextInt(iv.size())) + " since ");
		generateProperNoun(pn);
		repeatBecause = false;
		generateVerbPhrase(vp, tv, iv, np, dp, adjp, n, pn, adj, repeatADJP, repeatBecause);
	    }
	} else {
	    System.out.print(iv.get(iverbR.nextInt(iv.size())) + " ");
	}
    }
}